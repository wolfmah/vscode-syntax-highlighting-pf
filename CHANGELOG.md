# Change Log

All notable changes to the `pf-syntax` extension will be documented in this file.

## [Unreleased]
### Added
### Changed
### Removed
### Fix

## [0.3.0] - 2023-09-21
### Changed
- Updating plumbing + moving code hosting

## [0.2.0] - 2022-02-23
### Fix
- new regex pattern better allow to not match short keyword partially in long word
### Added
- adding icon to vscode marketplace

## [0.1.1] - 2022-02-23
### Fix
- Badges references in README.md
### Added
- References in README.md

## [0.1.0] - 2022-02-23
### Added
- Initial release
