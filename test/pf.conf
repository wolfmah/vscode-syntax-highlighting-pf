# comments #####################################################################
# TODO inside a comment
# A comment
block all # A comment
block all # TODO a comment
a = "# Not a comment" # But this is
b = "# TODO not a comment" # TODO But this is
################################################################################

# macros #######################################################################
a_a = "em0"
a_b= "em1"
a_c="em2"
block $a_b
################################################################################

# tables #######################################################################
table <BLOCK-perm> counters file "/tools/pf_block_permanent"
block all from <BLOCK-perm>
################################################################################

# constants ####################################################################
10.10.10.3
10.10.10.3/0
10.10.10.3/32
10.10.10.3/33           # FAIL
a=192.168.100.100
b=192.168.1000.1000     # FAIL
192.168.100.1000        # FAIL
::1
ff02::1
a=ff02::2               # Should not fail
a= ff02::2
2001:0db8:0123:4567:89ab:cdef:1234:5678
2001:0db8:0123:4567:89ab:cdef:1234:5678/0
2001:0db8:0123:4567:89ab:cdef:1234:5678/39
2001:0db8:0123:4567:89ab:cdef:1234:5678/119
2001:0db8:0123:4567:89ab:cdef:1234:5678/128
2001:0db8:0123:4567:89ab:cdef:1234:5678/129   # FAIL

abc all def
abc egress def

abc (egress:0) def
abc egress:broadcast def
abc egress:network def
abc (egress:peer) def
################################################################################

# filter actions ###############################################################
block all
match all
pass all

block drop return-icmp return-icmp6 return-rst return

a in b
a out b

a log b
a log all b
a log matches b
a log to asdf b
a log user b
a log all , matches b       # FAIL
a log all,matches) b        # FAIL
a log (all,matches b        # FAIL
a log (all) b
a log (all,matches) b
log ( all , matches , to asdf ) b
a log (user,to asdf,matches,all) b

a quick b

a on interface b
a on any b

a on rdomain 1 b

a os OpenBSD b
a os MadeUpOs     # FAIL

a port b          # FAIL
a port domain b
################################################################################

# options        ###############################################################
set block-policy drop
set block-policy return

set debug emerg
set debug alert
set debug crit
set debug err
set debug warning
set debug notice
set debug info
set debug debug

set fingerprints "path-to-file"

set hostid 789

set limit states 20000

set loginterface $IF_WLAN
set loginterface none

set optimization aggressive
set optimization conservative
set optimization high-latency
set optimization normal
set optimization satellite

set reassemble yes
set reassemble no
set reassemble yes no-df
set reassemble no no-df

set ruleset-optimization basic
set ruleset-optimization none
set ruleset-optimization profile

set skip on $IF_WLAN

set state-defaults pflow, no-sync

set state-policy if-bound
set state-policy floating

set syncookies never
set syncookies always
set syncookies adaptive (start 25%, end 12%)

set timeout tcp.first 120
set timeout tcp.established 86400
set timeout { adaptive.start 60000, adaptive.end 120000 }
################################################################################


################ OpenBSD pf.conf https://calomel.org ##########################
# Required order: options, queueing, translation and filtering.
#
################ Macros #######################################################

### Interfaces ###
 ExtIf ="em0"
 IntIf ="em1"

### Hosts ###
 Wraith  ="10.10.10.3"
 Xbox360 ="10.10.10.4"
 WorkSsh ="192.168.100.100"

### Queues, States and Types ###
 IcmpType ="icmp-type 8 code 0"
 IcmpMTUd ="icmp-type 3 code 4"
 SshQueue ="(ssh_bulk, ssh_login)"
#SynState ="flags S/SA synproxy state"
 TcpState ="flags S/SA modulate state"
 UdpState ="keep state"

### Ports ###
 FtpPort ="8021"
 SshPort ="8022"

### Stateful Tracking Options (STO) ###
 FtpSTO   ="(tcp.established 7200)"
 ExtIfSTO ="(max 9000, source-track rule, max-src-conn   2000, max-src-nodes 14)"
 IntIfSTO ="(max 150,  source-track rule, max-src-conn   50,   max-src-nodes 14, max-src-conn-rate 75/20)"
 SmtpSTO  ="(max 200,  source-track rule, max-src-states 50,   max-src-nodes 50, max-src-conn-rate 30/10,   overload <BLOCKTEMP> flush global)"
 SshSTO   ="(max 5,    source-track rule, max-src-states 5,    max-src-nodes 5,  max-src-conn-rate  5/60)"
 WebSTO   ="(max 500,  source-track rule, max-src-states 50,   max-src-nodes 75, max-src-conn-rate 120/100, overload <BLOCKTEMP> flush global)"

### Tables ###
 table <BLOCKTEMP> counters
 table <BLOCKPERM> counters file "/tools/pf_block_permanent"
 table <spamd-white>

################ Options ######################################################
### Misc Options
 set skip on lo
 set debug urgent
 set reassemble yes
 set block-policy drop
 set loginterface $ExtIf
 set state-policy if-bound
 set fingerprints "/etc/pf.os"
 set ruleset-optimization none

### Timeout Options for normal operations
 set optimization normal
 set timeout { tcp.established 600, tcp.closing 60 }

### Timeout Options for anti SYN DDoS with short timeouts and increased states
# set optimization aggressive
# set timeout { adaptive.end 120000, interval 2, tcp.tsdiff 5, tcp.first 5, tcp.closing 5, tcp.closed 5, tcp.finwait 5, tcp.established 4200}
# set limit   { states 100000, src-nodes 100000 }

################ Queueing ####################################################
### FIOS Upload = 20Mb/s (queue at 97%)
 altq on $ExtIf bandwidth 19.40Mb hfsc queue { ack, dns, ssh, web, mail, bulk, bittor, spamd }
  queue ack        bandwidth 30% priority 8 qlimit 500 hfsc (realtime   20%)
  queue dns        bandwidth  5% priority 7 qlimit 500 hfsc (realtime    5%)
  queue ssh        bandwidth 20% priority 6 qlimit 500 hfsc (realtime   20%) {ssh_login, ssh_bulk}
   queue ssh_login bandwidth 50% priority 6 qlimit 500 hfsc
   queue ssh_bulk  bandwidth 50% priority 5 qlimit 500 hfsc
  queue bulk       bandwidth 20% priority 5 qlimit 500 hfsc (realtime   20% default)
  queue web        bandwidth  5% priority 4 qlimit 500 hfsc (realtime  (10%, 10000, 5%) )
  queue mail       bandwidth  5% priority 3 qlimit 500 hfsc (realtime    5%)
  queue bittor     bandwidth  1% priority 2 qlimit 500 hfsc (upperlimit 95%)
  queue spamd      bandwidth  1% priority 1 qlimit 500 hfsc (upperlimit 1Kb)

################ Translation and Filtering ###################################

### Blocking spoofed packets: enable "set state-policy if-bound" above
#block drop in log quick on ! $ExtIf inet from (em0:network) to any

### Block to/from illegal sources/destinations
 block in     quick on $ExtIf inet proto tcp from <BLOCKTEMP> to any port != ssh
 block in     quick on $ExtIf inet proto tcp from <BLOCKPERM> to any port != ssh
 block in     quick on $ExtIf inet proto udp from <BLOCKTEMP> to any port != ssh
 block in     quick on $ExtIf inet proto udp from <BLOCKPERM> to any port != ssh
#block in     quick on $ExtIf inet           from any to 255.255.255.255
#block in log quick on $ExtIf inet           from urpf-failed to any
#block in log quick on $ExtIf inet           from no-route to any

### BLOCK all in on external interface by default and log
 block log on $ExtIf

### Network Address Translation (NAT with outgoing source port randomization)
 match out log on $ExtIf from  $Xbox360       to any received-on $IntIf tag EGRESS nat-to ($ExtIf:0) static-port
 match out log on $ExtIf from !$Xbox360       to any received-on $IntIf tag EGRESS nat-to ($ExtIf:0)

### Packet normalization ( "scrubbing" )
### remove "min-ttl 64" if you need native traceroute functions or just use "traceroute -I" instead
 match log on $ExtIf all scrub (random-id min-ttl 64 set-tos reliability reassemble tcp max-mss 1440)

### $ExtIf inbound
 pass in log on $ExtIf inet proto tcp  from !($ExtIf)      to ($ExtIf) port https $TcpState $WebSTO  queue (web, ack)  rdr-to lo0
 pass in log on $ExtIf inet proto tcp  from !($ExtIf)      to ($ExtIf) port www   $TcpState $WebSTO  queue (web, ack)  rdr-to lo0
 pass in log on $ExtIf inet proto tcp  from  <spamd-white> to ($ExtIf) port smtp  $TcpState $SmtpSTO queue (mail, ack) rdr-to lo0
 pass in log on $ExtIf inet proto tcp  from !<spamd-white> to ($ExtIf) port smtp  $TcpState $SmtpSTO queue (spamd)     rdr-to lo0 port spamd
 pass in log on $ExtIf inet proto tcp  from  $WorkSsh      to ($ExtIf) port ssh   $TcpState $SshSTO  queue $SshQueue   rdr-to lo0 port $SshPort
#pass in log on $ExtIf inet proto icmp from !($ExtIf)      to ($ExtIf) $IcmpType  $UdpState
#pass in log on $ExtIf inet proto icmp from !($ExtIf)      to ($ExtIf) $IcmpMTUd  $UdpState

### $ExtIf BitTorrent ( p2p )
 anchor "bittor" in on $ExtIf

### $ExtIf outbound
 pass out log on $ExtIf inet proto tcp  from ($ExtIf) to !($ExtIf)             $TcpState $ExtIfSTO queue (bulk, ack) tagged EGRESS
 pass out log on $ExtIf inet proto tcp  from ($ExtIf) to !($ExtIf) port ssh    $TcpState $ExtIfSTO queue $SshQueue   tagged EGRESS
 pass out log on $ExtIf inet proto tcp  from ($ExtIf) to !($ExtIf) port ftp    $TcpState $FtpSTO   queue (bulk)      tagged EGRESS
 pass out log on $ExtIf inet proto udp  from ($ExtIf) to !($ExtIf)             $UdpState $ExtIfSTO queue (bulk)      tagged EGRESS
 pass out log on $ExtIf inet proto udp  from ($ExtIf) to !($ExtIf) port domain $UdpState $ExtIfSTO queue (dns)       tagged EGRESS
 pass out log on $ExtIf inet proto icmp from ($ExtIf) to !($ExtIf)             $UdpState $ExtIfSTO queue (bulk)      tagged EGRESS

### $IntIf return (TCP reset) and log internal traffic
 block return log on $IntIf

### $IntIf inbound
 pass in log on $IntIf inet proto tcp  from  $IntIf:network to !$IntIf port www    $TcpState $ExtIfSTO
 pass in log on $IntIf inet proto tcp  from  $IntIf:network to !$IntIf port https  $TcpState $ExtIfSTO
 pass in log on $IntIf inet proto tcp  from  $IntIf:network to !$IntIf port ftp    $TcpState $IntIfSTO rdr-to    lo0 port $FtpPort  ##obsd 4.7 and earlier
#pass in log on $IntIf inet proto tcp  from  $IntIf:network to !$IntIf port ftp    $TcpState $IntIfSTO divert-to 127.0.0.1 port $FtpPort  ##obsd 5.1
 pass in log on $IntIf inet proto tcp  from  $Wraith        to  $IntIf port ssh    $TcpState $IntIfSTO rdr-to    lo0 port $SshPort
 pass in log on $IntIf inet proto udp  from  $IntIf:network to  $IntIf port domain $UdpState $IntIfSTO rdr-to    lo0
 pass in log on $IntIf inet proto udp  from  $IntIf:network to  $IntIf port ntp    $UdpState $IntIfSTO rdr-to    lo0
 pass in log on $IntIf inet proto udp  from  $IntIf:network to  $IntIf port bootps $UdpState $IntIfSTO
 pass in log on $IntIf inet proto icmp from  $IntIf:network to  $IntIf $IcmpType   $UdpState $IntIfSTO

### $IntIf ftp secure secure proxy for LAN 
 anchor "ftp-proxy/*" in on $IntIf inet proto tcp

### $IntIf outbound
 pass out log on $IntIf inet proto tcp  from $IntIf to $IntIf:network port ssh  $TcpState
 pass out log on $IntIf inet proto icmp from $IntIf to $IntIf:network $IcmpType $UdpState

### Games ( Xbox 360, PS3 and PC )
 anchor "games"

################ END of pf.conf https://calomel.org ###########################
