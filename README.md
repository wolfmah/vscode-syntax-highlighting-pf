# VS Code pf-syntax extension

[![Version Badge](https://img.shields.io/vscode-marketplace/v/wolfmah-vscode.pf-syntax.svg?style=flat-square&label=VS%20version)](https://marketplace.visualstudio.com/items?itemName=wolfmah-vscode.pf-syntax)
[![Installs Badge](https://img.shields.io/vscode-marketplace/i/wolfmah-vscode.pf-syntax.svg?style=flat-square&label=VS%20installs)](https://marketplace.visualstudio.com/items?itemName=wolfmah-vscode.pf-syntax)
[![Rating Badge](https://img.shields.io/vscode-marketplace/r/wolfmah-vscode.pf-syntax.svg?style=flat-square&label=VS%20rating)](https://marketplace.visualstudio.com/items?itemName=wolfmah-vscode.pf-syntax)<br/>
[![Version Badge](https://img.shields.io/open-vsx/v/wolfmah-vscode/pf-syntax?style=flat-square&label=OVSX%20version)](https://open-vsx.org/extension/wolfmah-vscode/pf-syntax)
[![Installs Badge](https://img.shields.io/open-vsx/dt/wolfmah-vscode/pf-syntax?style=flat-square&label=OVSX%20installs)](https://open-vsx.org/extension/wolfmah-vscode/pf-syntax)
[![Rating Badge](https://img.shields.io/open-vsx/rating/wolfmah-vscode/pf-syntax?style=flat-square&label=OVSX%20rating)](https://open-vsx.org/extension/wolfmah-vscode/pf-syntax)<br/>
[![License Badge](https://img.shields.io/badge/License-MPL%202.0-blue.svg?style=flat-square)](https://www.mozilla.org/en-US/MPL/2.0/)

VS Code extensions for syntax highlighting [PF](https://www.openbsd.org/faq/pf/index.html) configuration files.


## Usage

Binds to `*pf.conf` files.


## Development

### References

* [Syntax highlight guide](https://code.visualstudio.com/api/language-extensions/syntax-highlight-guide)
* [Text mate language grammars](https://macromates.com/manual/en/language_grammars)
* [man `pf.conf`](https://man.openbsd.org/pf.conf)
* [PF source code](https://github.com/openbsd/src/tree/master/sys/net) (mirror)

### Build

#### What's in the folder

* This folder contains all of the files necessary for yhe extension.
* `syntaxes/pf.tmLanguage.yaml` - this is the Text mate grammar file that is used for tokenization.
* `language-configuration.json` - this is the language configuration, defining the tokens that are used for comments and brackets.

#### Get up and running straight away

* `npm install`
* Make sure the language configuration settings in `language-configuration.json` are accurate.
* Press `F5` to open a new window with the extension loaded and a test `pf.conf` opened.
* Verify that syntax highlighting works and that the language configuration settings are working.

#### Make changes

* You can relaunch the extension from the debug toolbar after making changes to the files listed above.
* You can also reload (`Ctrl+R` or `Cmd+R` on Mac) the VS Code window with the extension to load your changes.

#### Install your extension

* To start using the extension with VS Code, copy it into the `<user home>/.vscode/extensions` folder and restart VS Code.


### Publish

* Change version
```
just version [major|minor|patch]
# or
npm version [major|minor|patch] --no-git-tag-version
```
* Update `CHANGELOG.md`
* Push changes upstream
* Create a tag
```
just tag
# or
git tag -a $(cat package.json | jq '.version' | cut -d '"' -f 2)
git push --tags
```
* Package the extension
```
just package
# or
npx @vscode/vsce package
```
* Publish the extension
    * [VS Marketplace: Publishing Extension](https://code.visualstudio.com/api/working-with-extensions/publishing-extension)
    * [Open VSX: Publishing Extensions](https://github.com/eclipse/openvsx/wiki/Publishing-Extensions)
```
just publish-vsmarket
just publish-ovsx
# or
npx @vscode/vsce publish --packagePath <file>
npx ovsx publish --packagePath <file>
```
